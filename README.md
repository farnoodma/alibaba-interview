# Alibaba Interview Question

This service will accept events alongside `user_id` and `score` from users and update the leaderboard.

## Table of Contents

* [Requirements](#requirements)
* [Setup - Building the Containers](#setup-building-the-containers)
* [Running the Service](#running-the-service)
* [Endpoints](#endpoints)
* [Testing](#testing)

## Requirements

In order to run this application with Docker you need to install it:

* Mac OSX via homebrew: `brew cask install docker`
* others: [https://docs.docker.com/engine/installation/](https://docs.docker.com/engine/installation/)

If you want to run it manually install python3.6 and then run the following command in the main directory of the project:  
```bash
pip install -r requirements.txt
```

## Setup - Building the Containers

You need to create the docker container from `Dockerfile`:

    docker build -t alibaba-interview -f ./Dockerfile .
    
## Running the Service

You need to run the service with following command:

    docker run -it alibaba-interview

## Endpoints

* **POST /event/**: Each user to create an event should call this endpoint. You must send a JSON with the following parameters: `user_id` and `score`.
* **GET /leaderboard/**: By calling this endpoint you will get the list of all users alongside their scores in the leaderboard.
* **WS /leaderboard/**: By connecting to this endpoint via websocket, you will first receive the entire leaderboard and then after each event creation you will receive the event to update the leaderboard.


## Testing

You can test the service with the following command:

    docker-compose -f ./docker-compose.test.yml build
    docker-compose -f ./docker-compose.test.yml run tester
    docker-compose -f ./docker-compose.test.yml down
