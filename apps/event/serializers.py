from rest_framework import fields, serializers

from apps.event.models import Event, Leaderboard


class EventSerializer(serializers.ModelSerializer):
    user_id = fields.IntegerField(min_value=1)

    class Meta:
        model = Event
        fields = ('user_id', 'score')

    def create(self, validated_data):
        event = Event(**validated_data)
        event.apply_score()
        return event


class LeaderboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Leaderboard
        fields = ('user_id', 'score', 'updated')
