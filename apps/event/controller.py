from apps.event.consumers import LeaderboardConsumer
from apps.event.models import Event, Leaderboard
from apps.event.serializers import EventSerializer, LeaderboardSerializer


class EventController:
    @staticmethod
    def leaderboard_controller():
        scores = Leaderboard.objects.order_by('-score')
        serializer = LeaderboardSerializer(scores, many=True)
        return serializer.data

    @staticmethod
    def event_controller(event_info: dict):
        serializer = EventSerializer(data=event_info)
        if serializer.is_valid():
            event: Event = serializer.save()
            LeaderboardConsumer.new_event(user_id=event.user_id, score=event.score)
            return {
                'status': 'OK'
            }
        else:
            return {
                'status': 'NOK',
                'error_messages': serializer.errors
            }
