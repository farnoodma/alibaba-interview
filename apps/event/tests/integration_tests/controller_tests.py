from collections import OrderedDict
from datetime import datetime
from unittest import mock

from django.test import TestCase

from apps.event.controller import EventController
from apps.event.models import Event, Leaderboard


class ControllerIntegrationTests(TestCase):
    def setUp(self):
        super().setUp()
        self.controller = EventController

    def test_leaderboardController_emptyLeaderboard_returnsEmptyList(self):
        result = self.controller.leaderboard_controller()

        self.assertListEqual([], result)

    def test_leaderboardController_notEmptyLeaderboard_returnsCorrectList(self):
        mocked_datetime = datetime(2020, 10, 20, 10, 30, 0, 0)
        mocked_str_datetime = mocked_datetime.strftime('%Y-%m-%dT%H:%M:%SZ')

        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_datetime)):
            Leaderboard.objects.bulk_create([
                Leaderboard(user_id=10, score=50),
                Leaderboard(user_id=5, score=10),
                Leaderboard(user_id=324, score=-100),
            ])

        result = self.controller.leaderboard_controller()

        self.assertListEqual([
            OrderedDict({'user_id': 10, 'score': 50, 'updated': mocked_str_datetime}),
            OrderedDict({'user_id': 5, 'score': 10, 'updated': mocked_str_datetime}),
            OrderedDict({'user_id': 324, 'score': -100, 'updated': mocked_str_datetime}),
        ], result)

    def test_eventController_wrongEventInfo_returnsNok(self):
        event_info = {'user_id': -20, 'score': 10}
        result = self.controller.event_controller(event_info)
        self.assertEqual('NOK', result['status'])

    def test_eventController_newUserEventInfo_returnsOkAndUpdateDatabase(self):
        event_info = {'user_id': 20, 'score': -10}
        result = self.controller.event_controller(event_info)
        self.assertEqual('OK', result['status'])
        self.assertEqual(-10, Event.objects.filter(user_id=20)[0].score)
        self.assertEqual(-10, Leaderboard.objects.get(user_id=20).score)

    def test_eventController_oldUserEventInfo_returnsOkAndUpdateDatabase(self):
        Event(user_id=20, score=50).apply_score()
        event_info = {'user_id': 20, 'score': -10}
        result = self.controller.event_controller(event_info)
        self.assertEqual('OK', result['status'])
        self.assertEqual(-10, Event.objects.filter(user_id=20)[1].score)
        self.assertEqual(40, Leaderboard.objects.get(user_id=20).score)
