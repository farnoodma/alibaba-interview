from django.test import TestCase

from apps.event.models import Event, Leaderboard


class EventUnitTests(TestCase):
    def setUp(self):
        super().setUp()

        self.user_id = 10
        Leaderboard.objects.create(user_id=self.user_id, score=30)
        self.event = Event(user_id=10, score=20)

    def test_applyScore_existingLeaderboard_updatesLeaderboard(self):
        user_score = Leaderboard.objects.get(user_id=self.user_id).score
        event_score = self.event.score

        self.event.apply_score()

        self.assertEqual(user_score + event_score, Leaderboard.objects.get(user_id=self.user_id).score)

    def test_applyScore_newUserID_createsLeaderboard(self):
        new_user_id = 40
        self.event.user_id = new_user_id
        event_score = self.event.score

        self.event.apply_score()

        self.assertEqual(event_score, Leaderboard.objects.get(user_id=new_user_id).score)
