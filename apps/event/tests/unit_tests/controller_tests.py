from collections import OrderedDict
from datetime import datetime
from unittest import mock

from django.test import TestCase

from apps.event.controller import EventController
from apps.event.models import Leaderboard


class ControllerUnitTests(TestCase):
    def setUp(self):
        super().setUp()
        self.controller = EventController

    @mock.patch('apps.event.controller.Leaderboard.objects.order_by')
    def test_leaderboardController_emptyQueryset_returnsEmptyList(self, queryset_mock):
        queryset_mock.return_value = []

        result = self.controller.leaderboard_controller()

        self.assertListEqual([], result)

    @mock.patch('apps.event.controller.Leaderboard.objects.order_by')
    def test_leaderboardController_notEmptyQueryset_returnsEmptyList(self, queryset_mock):
        mocked_datetime = datetime(2020, 10, 20, 10, 30, 0, 0)
        mocked_str_datetime = mocked_datetime.strftime('%Y-%m-%dT%H:%M:%SZ')
        queryset_mock.return_value = [
            Leaderboard(user_id=10, score=50, updated=mocked_datetime),
            Leaderboard(user_id=5, score=10, updated=mocked_datetime),
            Leaderboard(user_id=324, score=-100, updated=mocked_datetime),
        ]

        result = self.controller.leaderboard_controller()

        self.assertListEqual([
            OrderedDict({'user_id': 10, 'score': 50, 'updated': mocked_str_datetime}),
            OrderedDict({'user_id': 5, 'score': 10, 'updated': mocked_str_datetime}),
            OrderedDict({'user_id': 324, 'score': -100, 'updated': mocked_str_datetime}),
        ], result)

    @mock.patch('apps.event.models.Event.apply_score')
    def test_eventController_wrongEventInfo_returnsNok(self, apply_score_mock):
        event_info = {'user_id': -20, 'score': 10}
        result = self.controller.event_controller(event_info)
        self.assertEqual('NOK', result['status'])

    @mock.patch('apps.event.models.Event.apply_score')
    def test_eventController_correctEventInfo_returnsOk(self, apply_score_mock):
        event_info = {'user_id': 20, 'score': -10}
        result = self.controller.event_controller(event_info)
        self.assertEqual('OK', result['status'])
