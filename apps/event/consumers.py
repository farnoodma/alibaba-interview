import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels.layers import get_channel_layer

from apps.event.models import Leaderboard
from apps.event.serializers import LeaderboardSerializer


class LeaderboardConsumer(WebsocketConsumer):
    topic_name = 'leaderboard'

    @staticmethod
    def new_event(user_id, score):
        async_to_sync(get_channel_layer().group_send)(
            LeaderboardConsumer.topic_name,
            {
                'type': 'event_message',
                'message': {
                    'user_id': user_id,
                    'score': score
                }
            }
        )

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
            self.topic_name,
            self.channel_name
        )

        self.accept()
        self.send_entire_leaderboard()

    def send_entire_leaderboard(self):
        scores = Leaderboard.objects.order_by('-score')
        serializer = LeaderboardSerializer(scores, many=True)
        self.send(text_data=json.dumps(serializer.data))

    def event_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.topic_name,
            self.channel_name
        )
