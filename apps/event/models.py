from django.db import models, transaction
from django.db.models import F


class Event(models.Model):
    datetime = models.DateTimeField(auto_now_add=True, db_index=True)

    user_id = models.PositiveIntegerField(db_index=True)
    score = models.IntegerField()

    def apply_score(self):
        with transaction.atomic():
            self.save()
            update_count = Leaderboard.objects.filter(user_id=self.user_id).update(score=F('score') + self.score)
            if update_count <= 0:
                Leaderboard.objects.create(user_id=self.user_id, score=self.score)


class Leaderboard(models.Model):
    updated = models.DateTimeField(auto_now=True)

    user_id = models.PositiveIntegerField(unique=True)
    score = models.IntegerField(db_index=True)
