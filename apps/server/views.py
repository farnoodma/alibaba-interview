from rest_framework.response import Response
from rest_framework.views import APIView

from apps.event.controller import EventController


class LeaderboardViews(APIView):
    def get(self, *args, **kwargs):
        result = EventController.leaderboard_controller()
        return Response(result)


class EventViews(APIView):
    def post(self, request, *args, **kwargs):
        result = EventController.event_controller(request.data)
        return Response(result)
