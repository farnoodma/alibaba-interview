from django.urls import path

from apps.server.views import EventViews, LeaderboardViews

urlpatterns = [
    path('leaderboard/', LeaderboardViews.as_view()),
    path('event/', EventViews.as_view()),
]
