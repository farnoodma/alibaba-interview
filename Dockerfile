FROM python:3.7.6-slim-stretch

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install system wide dependencies
RUN apt-get update && apt-get install -y wait-for-it g++ gcc python3-dev musl-dev make cmake

# install python dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY . /usr/src/app/

EXPOSE 8000

# run Django
ENTRYPOINT python manage.py
CMD runserver 0.0.0.0:8000