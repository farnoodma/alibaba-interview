from channels.routing import ProtocolTypeRouter, URLRouter

import apps.event.routing

application = ProtocolTypeRouter({
    'websocket': URLRouter(
        apps.event.routing.websocket_urlpatterns
    ),
})
